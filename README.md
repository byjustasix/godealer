# GoDealer 1.1.1

GoDealer is a simple to use Godot addon for “simulating” decks of standard playing cards. This is a port of sorts of PyDealer.

## Setup
Inner Class
Copy the 'addon' folder in this project into the root folder of your Godot project and overwrite files if asked. Once that's done all you need to do is go to the 'Plugins' tab in 'Project Settings' and make sure the 'enable' box is ticked on for GoDealer. After that you are all set to use GoDealer!

## Quick Start

```
func _ready():
    var deck = Dealer.create_deck()
    # Note: By default 'create_deck' shuffles the deck
    deck.shuffle()
    var hand = deck.deal(7)
    hand.print()
```

## Setting Up Custom Suits, Ranks, and Extra Cards

The following would create a deck with six suits ('sun' and 'moon') with an extra rank called 'jester' that's smaller than a 'jack' but larger than a 'ten' that has another extra card called 'medium joker':

```
func _ready():
    Dealer.suits = ["moon", "sun", "hearts", "clubs", "diamonds", "spades"]
    Dealer.ranks = ["two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jester", "jack" "queen", "king", "ace"]
    Dealer.extra_cards = ["little_joker", "medium_joker", "big_joker"]
    var deck = GoDealer.create_deck()
```

Technically you can just use insert and append to add to those arrays, especially if you just want to add to the existing suits, ranks, or extra cards. Use the following line of code if you need to reset the suits, ranks, and extra cards to their defaults.

```
func _ready():
    Dealer.reset()
```