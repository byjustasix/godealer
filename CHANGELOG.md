# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.1] - 2024-04-03

### Fixed
* Card
    * Fixed the function name 'chnage' to 'change'

## [1.1.0] - 2024-04-03

### Added
* Card
    * Added a 'change' function to change the suit and rank at the same time
* Stack
    * Added a 'get_card' function that returns the card for the index given
    * Added a 'play_card' function that returns and removes the card for the index given

### Changed
* Stack
    * Change 'print_cards' function name to 'print_card_names'

## [1.0.0] - 2024-04-29

### Added
* Added GoDealer node
* Added Card inner class
* Added Stack inner class
* Added Deck inner class

## [MAJOR.MINOR.PATCH] - Year-Month-Date
### Added
* for new features

### Changed
* for changes in existing functionality

### Deprecated
* for soon-to-be removed features

### Removed
* for now removed features

### Fixed
* for any bug fixes

### Security
* in case of vulnerabilities


