extends Node2D


func _ready():
	var deck = Dealer.create_deck(false)
	var hand1 = deck.deal(13)
	var hand2 = deck.deal(13)
	var hand3 = deck.deal(13)
	var hand4 = deck.deal(13, true, true)
	print("------Player 1 Hand---------")
	hand1.print_cards()
	print("\n")
	print("------Player 2 Hand---------")
	hand2.print_cards()
	print("\n")
	print("------Player 3 Hand---------")
	hand3.print_cards()
	print("\n")
	print("------Player 4 Hand---------")
	hand4.print_cards()
	print("\n")
	print("------Deck---------")
	deck.print_cards()
