class_name GoDealer
extends Node
## GoDealer is a simple to use Godot addon for “simulating” decks 
## of standard playing cards. This is a port of sorts of PyDealer. [br]
## Example Usage:
## [codeblock]
## func _ready():
##     var deck = Dealer.create_deck()
##     # Note: By default 'create_deck' shuffles the deck
##     deck.shuffle()
##     var hand = deck.deal(7)
##     hand.print_cards()
## [/codeblock]

const default_suits : Array = ["hearts", "clubs", "diamonds", "spades"]
const default_ranks : Array = ["two", "three", "four", "five", "six",
"seven", "eight", "nine", "ten", "jack", "queen", "king", "ace"]
const default_extra_cards : Array = ["little_joker", "big_joker"]

## An array of every suit for your cards in descending order.
var suits : Array = default_suits
## An array of every rank for your cards in descending order.
var ranks : Array = default_ranks
## An array of every special card (ie jokers) for your cards
## in descending order.
var extra_cards : Array = default_extra_cards

## Resets the suits, ranks, and extra cards to their defaults
func reset():
	suits = default_suits
	ranks = default_ranks
	extra_cards = default_extra_cards

## Creates and returns a card of the given suit and rank
func create_card(suit : int, rank : int) -> Card:
	var card = Card.new(suit, rank)
	return card

## Creates and returns an empty stack unless a single card,
## a stack, or an array of cards is passed in which are added
## to the new stack.
func create_stack(cards_to_add = false) -> Stack:
	var stack = Stack.new()
	
	if cards_to_add is Card or cards_to_add is Stack:
		stack.add(cards_to_add)
	elif cards_to_add is Array[Card]:
		stack.cards.append_array(cards_to_add)

	return stack

## Creates and returns a deck. By default the deck will
## have both jokers and be shuffled.
func create_deck(add_extras : bool = true, shuffle_deck : bool = true) -> Deck:
	var deck = Deck.new(add_extras)
	
	if shuffle_deck:
		deck.shuffle()
		
	return deck

## Each Card instance represents a single playing card, 
## of a given value and suit.
class Card:
	extends Resource
	
	## An int representing the card's suit
	var suit : int = 0 :
		set(value):
			if (value >= -1 and value < Dealer.suits.size()):
				suit = value
	
	## An int representing the card's rank
	var rank : int = 0 :
		set(value):
			if (value > 0 and value < Dealer.ranks.size()):
				rank = value
	
	
	func _init(suit : int = 0, rank : int = 0):
		change(suit, rank)
	
	## Changes the suit and rank of a card at the same time
	func change(suit : int, rank : int):
		self.suit = suit
		self.rank = rank
	
	## Returns the card's name ('Rank' of 'Suit')
	func get_card_name() -> String:
		if suit >= 0:
			return str(Dealer.ranks[rank], " of ", Dealer.suits[suit])
		else:
			return Dealer.extra_cards[rank].replace("_", " ")
	
	## Prints the card's name
	func print_name():
		print(get_card_name())

## The Stack class is the backbone of GoDealer. 
## A Stack is essentially just a generic “card container”, 
## with all of the functions users may need to work with the 
## cards they contain. A Stack can be used as a hand, or a 
## discard pile, etc.
class Stack:
	extends Resource
	
	## An array containing every card in the stack
	var cards : Array[Card] = []
	
	# Missing Function from Pydealer
	# find, find_list, get, get_list
	# inset, insert_list, is_sorted, 
	# open, save, sort, split
	
	## Adds given card(s) to the stack.
	## 'new_cards' can be passed in as a
	## single card or another stack
	func add(new_cards, to_top : bool = false):
		var list = []
		
		if new_cards is Card:
			list.append(new_cards)
		elif new_cards is Stack:
			list = new_cards.cards
		
		for card in list:
			if to_top:
				cards.push_front(card)
			else:
				cards.append(card)
	
	## Draws cards from the top or bottom of the stack
	## and returns them as another stack.
	func deal(num : int = 1, from_top : bool = true) -> Stack:
		if num >= cards.size():
			return empty(true)
		
		var stack = Stack.new()
		
		for i in num:
			if from_top:
				stack.add(cards.pop_front())
			else:
				stack.add(cards.pop_back())
		
		return stack
	
	## Removes all cards from the stack. Passing
	## in true will return the cards in the stack
	## as another stack
	func empty(return_cards : bool = false):
		if return_cards:
			var stack = Stack.new()
			stack.add(cards)
			cards.clear()
			return stack
		else:
			cards.clear()
	
	## Returns card from stack
	func get_card(index) -> Card:
		return cards[index]
	
	## Returns and removes card from stack
	func play_card(index) -> Card:
		return cards.pop_at(index)
	
	## Prints the name of every card in the stack
	func print_card_names():
		for card in cards:
			card.print_name()
	
	## Returns a random card. Passing in true will
	## remove the card from the stack.
	func random_card(remove : bool = false) -> Card:
		var card = cards.pick_random()
		if remove:
			cards.erase(card)
		return card
	
	## Reverses the order of the cards in the stack.
	func reverse():
		cards.reverse()
	
	## Sets the cards in this stack to the cards
	## in the given stack.
	func set_cards(cards : Stack):
		self.cards = cards.cards
	
	## Returns the number of cards in the stack.
	func size() -> int:
		return cards.size()
	
	## Shuffles the order of the cards in the stack.
	func shuffle(times : int = 1):
		for i in times:
			cards.shuffle()

## Each Deck instance, by default, contains a full,
## 52 card French deck of playing cards upon instantiation. 
## The Deck class is a subclass of the [GoDealer.Stack] class, with a 
## few extra/differing methods.
class Deck:
	extends Stack
	
	## Determines if deck builds with extra cards
	var has_extras : bool = true
	
	
	func _init(add_extras : bool = true):
		build(add_extras)
		
	# Many add some options to add only one joker
	## Creates a card for every rank for every suit found in
	## GoDealer's 'suits' and 'ranks' array and adds them to
	## the deck. By default this will add an extra card for every
	## entry in GoDealer's 'extra_cards' array
	func build(add_extras : bool = true):
		has_extras = add_extras
		for s in Dealer.suits.size():
			for r in Dealer.ranks.size():
				var card = Card.new(s, r)
				cards.append(card)
		if add_extras:
			for e in Dealer.extra_cards.size():
				var card = Card.new(-1, e)
				cards.append(card)
	
	## Works similarily to Stack's deal function but parameters have been
	## added to rebuild the deck if cards run out and/or whether to shuffle 
	## the deck after dealing the cards.
	func deal(num = 1, from_top = true, rebuild = false, shuffle_deck = false) -> Stack:
		var stack = Stack.new()
		
		if num >= cards.size():
			stack.add(empty(true))
			if rebuild:
				build(has_extras)
		else:
			for i in num:
				if from_top:
					stack.add(cards.pop_front())
				else:
					stack.add(cards.pop_back())
		
		if shuffle_deck:
			shuffle()
		
		return stack
